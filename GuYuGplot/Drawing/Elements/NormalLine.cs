﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GuYuGplot.Drawing.Elements
{
    class NormalLine:AbsLineBase,IDrawing
    {
        #region IDrawing 成员

        public void Draw(Graphics g)
        {
            g.DrawLine(new Pen(this.Color,this.Width), this.StartLocation, this.EndLocation);
        }

        #endregion
    }
}
