﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GuYuGplot.Entites;

namespace GuYuGplot.Drawing.Elements
{
    class LinesFactory
    {
        public AbsLineBase GetLine(Enums.LineType lineType)
        {
            switch (lineType)
            {
                case Enums.LineType.Normal:
                    return new NormalLine();
                case Enums.LineType.BlockLine:
                    return new BlockLine();
                default:
                    return new NormalLine();
            }
        }
    }
}
