﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GuYuGplot.Drawing.Elements
{
    abstract class AbsLineBase
    {
        public PointF StartLocation { get; set; }
        public PointF EndLocation { get; set; }
        public float Width { get; set; }
        public Color Color { get; set; }
    }
}
