﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GuYuGplot.Entites;
using System.Drawing;
using GuYuGplot.Drawing.Elements;

namespace GuYuGplot.Drawing
{
    class DrawingOperator
    {
        private float _canvasWidth; //画布宽度
        private float _canvasHeight;//画布高度
        private NodesFactory _nodesFactory = new NodesFactory();
        private LinesFactory _linesFactory = new LinesFactory();
        private List<IDrawing> _elementList;//要绘制的元素

        /// <summary>
        /// 根据参数进行绘制
        /// </summary>
        /// <param name="nodesTree">要绘制的节点树</param>
        /// <param name="graphics">画笔</param>
        /// <param name="canvasWidth">画布宽度</param>
        /// <param name="canvasHeight">画布高度</param>
        public void Draw(NodesTree nodesTree, Graphics graphics, float canvasWidth, float canvasHeight)
        {
            if (nodesTree.RootNode != null)
            {
                _canvasHeight = canvasHeight;
                _canvasWidth = canvasWidth;
                _elementList = this.GetDrawingElementList(nodesTree.RootNode);
                foreach (var item in _elementList)
                {
                    item.Draw(graphics);
                }
            }           
        }

        //将节点及子孙节点转为为可绘制元素
        private List<IDrawing> GetDrawingElementList(Node rootNode)
        {
            List<IDrawing>  list = new List<IDrawing>();

            //跟节点放置于画布中心
            float rootNodeX = _canvasWidth / 2;
            float rootNodeY = _canvasHeight / 2;
      
            //参数指定节点的所有子节点连接线（函数内部递归调用自身以得到所有的连线绘制元素）
            this.Convert2DrawingLineElement(rootNode, rootNodeX, rootNodeY, ref list);

            //跟节点
            AbsNodeBase drawingRootNode = _nodesFactory.GetNode(rootNode.NodeType);
            drawingRootNode.Color = rootNode.Color;
            drawingRootNode.Location = new PointF(rootNodeX - rootNode.Width / 2, rootNodeY - rootNode.Height / 2);
            drawingRootNode.Size = new SizeF(rootNode.Width, rootNode.Height);
            drawingRootNode.Text = rootNode.Text;
            drawingRootNode.NodeName = rootNode.Name;
            drawingRootNode.TextColor = rootNode.TextColor;
            drawingRootNode.TextSize = rootNode.TextSize;
            list.Add(drawingRootNode as IDrawing);

            //参数指定节点的所有子节点（函数内部递归调用自身以得到所有节点的绘制元素）
            this.Convert2DrawingNodeElement(rootNode, rootNodeX, rootNodeY, ref list);

            return list;
        }

        //将指定节点的所有子节点连线转换为可绘制的元素
        private void Convert2DrawingLineElement(Node rootNode, float rootNodeX, float rootNodeY, ref List<IDrawing> list)
        {
            if (rootNode.ChildNodes.Count > 0)
            {
                float intervalAngle = 360f / rootNode.ChildNodes.Count;//相邻节点之间的夹角
                for (int i = 0; i < rootNode.ChildNodes.Count; i++)
                {
                    GuYuGplot.Entites.Line line = rootNode.ChildNodes[i].Line;
                    if (line != null)
                    {
                        float angle = i * intervalAngle;
                        float startX = rootNodeX;
                        float startY = rootNodeY;
                        float endX = rootNodeX + rootNode.ChildNodes[i].Line.Length * (float)Math.Cos(angle / 180 * Math.PI);//  angle / 180 * Math.PI 作用是将角度转化为弧度
                        float endY = rootNodeY - rootNode.ChildNodes[i].Line.Length * (float)Math.Sin(angle / 180 * Math.PI);

                        AbsLineBase drawingLine = _linesFactory.GetLine(line.LineType);
                        drawingLine.Color = line.Color;
                        drawingLine.StartLocation = new PointF(startX, startY);
                        drawingLine.EndLocation = new PointF(endX, endY);
                        drawingLine.Width = line.Width;
                        list.Add(drawingLine as IDrawing);
                        this.Convert2DrawingLineElement(rootNode.ChildNodes[i], endX, endY, ref list);
                    }                    
                }
            }
        }

        //将指定节点的所有子节点转换为可绘制的元素
        private void Convert2DrawingNodeElement(Node rootNode, float rootNodeX, float rootNodeY, ref List<IDrawing> list)
        {
            if (rootNode.ChildNodes.Count > 0)
            {
                float intervalAngle = 360f / rootNode.ChildNodes.Count;//相邻节点之间的夹角
                for (int i = 0; i < rootNode.ChildNodes.Count; i++)
                {
                    Node childNode = rootNode.ChildNodes[i];
                    if (childNode.Line != null)
                    {
                        PointF location = new PointF();
                        location.X = rootNodeX + childNode.Line.Length * (float)Math.Cos(i * intervalAngle / 180 * Math.PI) - childNode.Width / 2;
                        location.Y = rootNodeY - childNode.Line.Length * (float)Math.Sin(i * intervalAngle / 180 * Math.PI) - childNode.Height / 2;

                        AbsNodeBase drawingNode = _nodesFactory.GetNode(childNode.NodeType);
                        drawingNode.Color = childNode.Color;
                        drawingNode.Location = location;
                        drawingNode.Size = new SizeF(childNode.Width, childNode.Height);
                        drawingNode.Text = childNode.Text;
                        drawingNode.NodeName = childNode.Name;
                        drawingNode.TextColor = childNode.TextColor;
                        drawingNode.TextSize = childNode.TextSize;
                        list.Add(drawingNode as IDrawing);
                        this.Convert2DrawingNodeElement(childNode, location.X + childNode.Width / 2, location.Y + childNode.Width / 2, ref list);
                    }
                }
            }            
        }

        /// <summary>
        /// 给定点是否在节点内部
        /// </summary>
        /// <param name="x">横坐标</param>
        /// <param name="y">纵坐标</param>
        /// <param name="nodeName">对应节点名称</param>
        /// <returns>true or false</returns>
        internal bool InNode(int x, int y, out string nodeName)
        {
            bool result = false;
            nodeName = null;
            if (_elementList != null)
            {
                foreach (var item in _elementList)
                {
                    if (item is AbsNodeBase)
                    {
                        AbsNodeBase node = item as AbsNodeBase;
                        if (x > node.Location.X &&
                            x < node.Location.X + node.Size.Width &&
                            y > node.Location.Y &&
                            y < node.Location.Y + node.Size.Height)
                        {
                            nodeName = node.NodeName;
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }
    }
}
