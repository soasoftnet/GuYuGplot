﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GuYuGplot.Entites
{
    public class Node
    {
        public String Text { get; set; }

        private int _textSize = 12;
        public int TextSize
        {
            get { return _textSize; }
            set { _textSize = value; }
        }

        private Color _textColor = Color.White;
        public Color TextColor
        {
            get { return _textColor; }
            set { _textColor = value; }
        }

        public String Name { get; set; }

        private Color _color = Color.Yellow;
        public Color Color
        {
            get { return _color; }
            set { _color = value; }
        }

        public float Width { get; set; }

        public float Height { get; set; }

        public Enums.NodeType NodeType { get; set; }

        private List<Node> _childNodes = new List<Node>();
        public List<Node> ChildNodes
        {
            get { return _childNodes; }
            set { _childNodes = value; }
        }

        public Line Line { get; set; }
    }
}
