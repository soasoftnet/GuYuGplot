﻿
/********************** How to use ************************************/
/*                                                                    */
/* step 0 : 添加跟节点，根节点的Line属性不需赋值                      */
/* setp 1 : 添加跟节点的子节点，需要对子节点的Line属性（连接线）赋值  */
/* setp 2 : 添加子节点的子节点......                                  */
/* 还有两个点击事件，事件参数中包括节点名称（最好使用不重复的节点命名)*/
/*                                                                    */
/*                                              2011-8-9 北京 Joey    */
/*                                                                    */
/**********************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GuYuGplot.Entites;

namespace TestMain
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            this.InitializeComponent();
            this.InitTestData();
            this.gplot1.NodeDoubleClick += new EventHandler<GuYuGplot.NodeClickEventArgs>(gplot1_NodeDoubleClick);
        }

        void gplot1_NodeDoubleClick(object sender, GuYuGplot.NodeClickEventArgs e)
        {
            MessageBox.Show(e.NodeName);
        }

        private void InitTestData()
        {
            //
            //跟节点
            //
            Node rootNode = new Node();
            rootNode.Color = Color.Blue;
            rootNode.Height = 40;
            rootNode.Width = 75;
            rootNode.NodeType = Enums.NodeType.Rect;
            rootNode.Text = "Janice";
            rootNode.Name = "Root";
            rootNode.TextColor = Color.Red;
            rootNode.TextSize = 15;

            //
            //第二级节点
            //
            for (int i = 0; i < 3; i++)
            {
                Color color = Color.Brown;
                int length = 200;
                Node childNode = new Node();
                childNode.Color = color;
                childNode.Height = 40;
                childNode.Width = 50;
                childNode.NodeType = Enums.NodeType.Circle;
                childNode.TextColor = Color.Blue;
                childNode.TextSize = 12;
                childNode.Text = "God";
                childNode.Name = "Child_L1_" + i.ToString();

                Line line = new Line();
                line.Color = color;
                line.Length = length;
                line.Width = 3;
                line.LineType = Enums.LineType.Normal;

                childNode.Line = line;

                rootNode.ChildNodes.Add(childNode);

                //
                //第三级节点
                //
                for (int j = 0; j < 3; j++)
                {
                    Node childNode2 = new Node();
                    childNode2.Color = Color.Red;
                    childNode2.Height = 40;
                    childNode2.Width = 40;
                    childNode2.NodeType = Enums.NodeType.Circle;
                    childNode2.Text = "my";
                    childNode2.Name = "Child_L2_" + j.ToString();
                    childNode2.TextColor = Color.White;
                    childNode2.TextSize = 10;
                    childNode2.Line = new Line();
                    childNode2.Line.Color = Color.Red;
                    childNode2.Line.Width = 2;
                    childNode2.Line.LineType = Enums.LineType.Normal;
                    childNode2.Line.Length = 100;
                    childNode.ChildNodes.Add(childNode2);

                    //
                    //第四级节点
                    //
                    for (int q = 0; q < 3; q++)
                    {
                        Node childNode3 = new Node();
                        childNode3.Color = Color.Yellow;
                        childNode3.Height = 30;
                        childNode3.Width = 30;
                        childNode3.NodeType = Enums.NodeType.Circle;
                        childNode3.Text = "oh";
                        childNode3.Name = "Child_L3_" + q.ToString();
                        childNode3.TextColor = Color.White;
                        childNode3.TextSize = 8;
                        childNode3.Line = new Line();
                        childNode3.Line.Color = Color.Green;
                        childNode3.Line.Width = 1;
                        childNode3.Line.LineType = Enums.LineType.Normal;
                        childNode3.Line.Length = 50;
                        childNode2.ChildNodes.Add(childNode3);
                    }
                }
            }
            this.gplot1.NodesTree.RootNode = rootNode;
        }
    }
}
